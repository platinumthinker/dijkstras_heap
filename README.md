## Description
Implementation dijkstras on heap on pure c

## Complexity:
#n * log(n) + m * log(n), where
m - count of edge
n - count of vertex

##Link on very verbose algorithm description:
[wiki](http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm)
##Illustrate algorithm:
![Wikipedia](http://upload.wikimedia.org/wikipedia/commons/5/57/Dijkstra_Animation.gif)
![Wikipedia](http://upload.wikimedia.org/wikipedia/commons/2/23/Dijkstras_progress_animation.gif)
