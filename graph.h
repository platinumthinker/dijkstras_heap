#include <stdio.h>
#include <stdlib.h>
#pragma once

struct graph
{
    unsigned long vertex_count, edge_count;
    char **g;
};

struct graph *read_graph(const char *name, unsigned long id);
struct graph *fill_random_graph(unsigned long count, double persent);
/* void add_edge(struct vertex_graph *a, struct vertex_graph *b, int weight); */
