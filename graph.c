#include "graph.h"
#include <time.h>

#define MAX_GRAPH_WEIGHT 127

//DATA:
//count_vertex count_edge
//vertexID vertexID vertexCost

struct graph *read_graph(const char *name, unsigned long id)
{
    FILE *f = fopen(name, "r");
    unsigned long vertex_count, edge_count, i, j;
    struct graph *g;

    fscanf(f, "%lu %lu\n", &vertex_count, &edge_count);

    char **e = (char**)malloc(sizeof(char*) * vertex_count);
    for (i = 0; i < vertex_count; i++)
    {
        e[i] = (char *)malloc(sizeof(char) * vertex_count);
        for (j = 0; j < vertex_count; j++)
            e[i][j] = -1;
    }

    for (i = 0; i < edge_count; i++)
    {
        unsigned long A, B;
        int w;
        fscanf(f, "%lu %lu %d", &A, &B, &w);
        A--, B--;
        e[A][B] = w;
        e[B][A] = w;
    }

    g = (struct graph*)malloc(sizeof(struct graph));
    g->g = e;
    g->vertex_count = vertex_count;
    g->edge_count = edge_count;

    fclose(f);

    return g;
}

void shuffle(unsigned long *array, unsigned long n)
{
    if (n > 1)
    {
        unsigned long i;
        for (i = 0; i < n - 1; i++)
        {
          size_t j = i + rand() / (RAND_MAX / (n - i) + 1);
          unsigned long t = array[j];
          array[j] = array[i];
          array[i] = t;
        }
    }
}

/* struct graph *fill_random_graph(unsigned long count, double persent) */
/* { */
/*     struct vertex_graph ***v; */
/*     struct graph *g; */
/*     unsigned long edge_count = 0, vertex_count, i, z, j; */

/*     vertex_count = count * count; */
/*     v = (struct vertex_graph ***)malloc(size_p * count); */

/*     for (i = 0; i < count; i++) */
/*     { */
/*         v[i] = (struct vertex_graph**)malloc(size_p * count); */
/*         for (j = 0; j < count; j++) */
/*         { */
/*             v[i][j] = (struct vertex_graph*)malloc(size_s * count); */
/*             v[i][j]->id = i*count + j; */
/*             v[i][j]->count = 0; */
/*             v[i][j]->vertexes = (struct vertex_graph**)malloc(size_s * 4); */
/*             v[i][j]->weight = (int*)malloc(sizeof(int) * 4); */
/*         } */
/*     } */

/*     unsigned long seed = time(NULL); */
/*     srand(seed); */

/*     for (i = 0; i < count; i++) */
/*     { */
/*         int w; */
/*         for (j = 0; j < count; j++) */
/*         { */
/*             w = rand() % MAX_GRAPH_WEIGHT; */
/*             z = (j + 1) % count; */
/*             add_edge(v[i][j], v[i][z], w); */
/*             printf("Edge: %lu %lu w: %d\n", v[i][j]->id + 1, v[i][z]->id + 1, w); */
/*             w = rand() % MAX_GRAPH_WEIGHT; */
/*             z = (i + 1) % count; */
/*             add_edge(v[i][j], v[z][j], w); */
/*             printf("Edge: %lu %lu w: %d\n", v[i][j]->id + 1, v[z][j]->id + 1, w); */
/*             edge_count += 2; */
/*         } */
/*     } */

/*     g = (struct graph*)malloc(sizeof(struct graph)); */
/*     g->v = v[0][0]; */
/*     g->vertex_count = vertex_count; */
/*     g->edge_count = edge_count; */

/*     for (i = 0; i < count; i++) */
/*         free(v[i]); */
/*     free(v); */

/*     return g; */
/* } */
