dijkstras: main.c graph.c
	gcc main.c graph.c -g -O3 -Wall -std=c11 -o dijkstras 

clean: dijkstras
	rm dijkstras
