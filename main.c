#include "graph.h"
#include <string.h>

#define percent 0.10 // if 1.00 edge in graph = N*N

void get_res(struct graph *g, unsigned long st)
{
   int w_b;
   unsigned long cc = 0, i;
   char *mark;

   int *w = (int *)malloc(sizeof(int) * g->vertex_count);
   mark = (char *)malloc(sizeof(char) * g->vertex_count);
   for (unsigned long i = 0; i < g->vertex_count; i++)
   {
       w[i] = -1;
       mark[i] = 0;
   }

   w[st] = 0;

   while (cc != g->vertex_count)
   {
       st = -1;
       for (i = 0; i < g->vertex_count; i++)
       {
           if (!mark[i] && (st == -1 || (w[i] != -1 && w[i] < w[st])))
               st = i;
       }
       printf("Select id%lu\n", st);

       mark[st] = 1;
       cc++;

       for (i = 0; i < g->vertex_count; i++)
       {
           if (mark[i] || g->g[st][i] == -1)
               continue;

           w_b = g->g[st][i] + w[st];
           if (w[i] > w_b || w[i] < 0)
           {
               w[i] = w_b;
               printf("New cost: %d for id%lu\n", w_b, i);
           }
       }
   }

   for (unsigned long i = 0; i < g->vertex_count; i++)
       printf("id %ld cost: %d\n", i + 1, w[i]);

   free(w);
}

int main(int argc, const char *argv[])
{
    long idStart;
    struct graph *g;
    switch (argc)
    {
        /* case 2: */
        /*     idStart = atol(argv[1]); // Count in random graph */
        /*     g = fill_random_graph(idStart, percent); */
        /*     break; */
        case 3:
            idStart = atol(argv[2]) - 1;
            g = read_graph(argv[1], idStart);
            break;
        default:
            printf("Use: ./dijkstras datafie V_id\n");
            printf(" or: ./dijkstras N\n\n");
            return 1;
    }

    get_res(g, idStart);
    free(g);
}
